import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/routes/app_pages.dart';
import 'package:flutter_getx_scheleton_app/app/routes/app_routes.dart';
import 'package:flutter_getx_scheleton_app/app/themes/app_themes.dart';
import 'package:flutter_getx_scheleton_app/app/translations/app_translations.dart';
import 'package:get/get.dart';
import 'dart:ui' as ui;

import 'helpers/constants.dart';

class ScheletonApp extends StatelessWidget {
  //  Metodo per nascondere la tastiera quando si fa tap
  void hideKeyboard(BuildContext context) {
    // final currentFocus = FocusScope.of(context);
    // if(currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null){
    //   FocusManager.instance.primaryFocus!.unfocus();
    //   //(FocusManager.instance.primaryFocus as FocusNode).unfocus();
    // }
    FocusManager.instance.primaryFocus?.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: appName,
      debugShowCheckedModeBanner: false,
      //  Routing
      getPages: AppPages.pages,
      initialRoute: AppRoutes.INIT,
      //  Theme Data
      //theme: lightTheme,
      theme: lightTheme,
      //  Localization
      locale: ui.window.locale.languageCode == 'it'
          ? const Locale('it', 'IT')
          : const Locale('en', 'US'),
      fallbackLocale: const Locale('en', 'US'),
      translationsKeys: AppTranslations.translations,
      //  GestureDetector per chiudere la tastiera quando si fa tap sullo schermo
      builder: (context, child) => GestureDetector(
        onTap: () {
          hideKeyboard(context);
        },
        child: child,
      ),
    );
  }
}