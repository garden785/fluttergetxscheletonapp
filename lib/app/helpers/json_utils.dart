class JsonUtils {
  static dynamic convertJsonField({
    required dynamic inputValue,
    required String outputType,
    dynamic outputDefaultValue,
    dynamic booleanPositiveCheck,
  }) {
    dynamic output = outputDefaultValue;
    if (outputType.isNotEmpty) {
      outputType = outputType.trim().toLowerCase();
      switch (outputType) {
        case 'string':
          output = inputValue != null ? inputValue.toString() : output;
          break;
        case 'int':
          if (inputValue != null) {
            if (inputValue is int) {
              output = inputValue;
            } else if (inputValue is double) {
              output = inputValue.floor();
            } else if (inputValue is String && inputValue.isNotEmpty) {
              dynamic tempParse = num.tryParse(inputValue);
              if (tempParse != null) {
                if (tempParse is double) {
                  output = tempParse.floor();
                } else {
                  output = tempParse;
                }
              }
            }
          }
          break;
        case 'double':
          if (inputValue != null) {
            if (inputValue is int) {
              output = inputValue.toDouble();
            } else if (inputValue is double) {
              output = inputValue;
            } else if (inputValue is String && inputValue.isNotEmpty) {
              dynamic tempParse = num.tryParse(inputValue);
              if (tempParse != null) {
                if (tempParse is double) {
                  output = tempParse;
                } else {
                  output = tempParse.toDouble();
                }
              }
            }
          }
          break;
        case 'bool':
          if (inputValue != null) {
            if (inputValue is bool) {
              output = inputValue;
            } else if (inputValue is int) {
              output = inputValue == booleanPositiveCheck;
            } else if (inputValue is double) {
              output = inputValue.floor() == booleanPositiveCheck;
            } else if (inputValue is String && inputValue.isNotEmpty) {
              if (inputValue.toLowerCase() == 'true') {
                output = true;
              } else if (inputValue == booleanPositiveCheck) {
                output = true;
              } else {
                output = false;
              }
            }
          }
          break;
        case 'datetime':
          if (inputValue != null &&
              inputValue is String &&
              inputValue.isNotEmpty) {
            output = DateTime.tryParse(inputValue);
          }
          break;
      }
    }
    return output;
  }
}
