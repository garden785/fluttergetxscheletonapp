import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LocalUtils{
  static void showError(String title, String message){
    Get.snackbar(
        title,
        message,
        snackPosition: SnackPosition.BOTTOM,
        isDismissible: true,
        backgroundColor: Colors.black,
        colorText: Colors.white,
        borderRadius: 0,
        leftBarIndicatorColor: Colors.red,
        borderWidth: 5,
        icon: const Icon(Icons.error_outline, color: Colors.red)
    );
  }
}