import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UIHelper {
  static Widget vSpace(double space) {
    return Container(
      height: space,
    );
  }

  static Widget hSpace(double space) {
    return Container(
      width: space,
    );
  }

  static showLoadingDialog() {
    showDialog(
      context: Get.overlayContext!,
      barrierDismissible: false,
      builder: (_) => WillPopScope(
        onWillPop: () async => false,
        child: Center(
          child: SizedBox(
            width: 60,
            height: 60,
            child: CircularProgressIndicator(
              strokeWidth: 10,
            ),
          ),
        ),
      ),
    );
  }

  static hideLoadingDialog() {
    Navigator.of(Get.overlayContext!).pop();
  }
}
