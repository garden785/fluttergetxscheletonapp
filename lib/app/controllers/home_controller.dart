import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/controllers/login_controller.dart';
import 'package:flutter_getx_scheleton_app/app/data/models/country_model.dart';
import 'package:flutter_getx_scheleton_app/app/data/repositories/states_repository.dart';
import 'package:flutter_getx_scheleton_app/app/helpers/local_utils.dart';
import 'package:flutter_getx_scheleton_app/app/routes/app_routes.dart';
import 'package:get/get.dart';

class HomeController extends GetxController with SingleGetTickerProviderMixin {
  final _loginController = Get.find<LoginController>();

  //  Properties
  late TabController tabController;
  final _navigationQueue = ListQueue<int>();
  bool _saveNavigationStack = true;
  RxList<Country> _countries = RxList();

  //  Getters and setters
  LoginController get loginController => _loginController;
  bool get hasNoNavigationInQueue => _navigationQueue.isEmpty;
  List<Country> get countries => _countries;

  @override
  void onInit() {
    initTabBarController();
    print('HomeController - onInit');
    super.onInit();
    if(_loginController.user.id == 0){
      Get.offAllNamed(AppRoutes.INIT);
    }
  }

  @override
  void onReady() {
    print('HomeController - onReady');
    super.onReady();
  }

  @override
  void onClose() {
    print('HomeController - onClose');
    super.onClose();
  }

  @override
  void dispose() {
    print('HomeController - dispose');
    this.tabController.dispose();
    super.dispose();
  }



  /// Custom Methods
  void initTabBarController(){
    int tabBarItemCount = 3;
    tabController = TabController(length: tabBarItemCount, initialIndex: 0, vsync: this);
    tabController.addListener(_handleTabChange);
  }

  // event that manage the changing of tab in tabController.
  // since this method can be called more times during a single change,
  // I need to check the navigation stack
  void _handleTabChange() {
    if (this._saveNavigationStack && (hasNoNavigationInQueue || (_navigationQueue.last != tabController.previousIndex))) {
      _navigationQueue.addLast(tabController.previousIndex);
    }
    this._saveNavigationStack = true;
  }

  // go back in page using the latest item in
  // navigation queue
  goBackNavigation() {
    // security check
    if (this.hasNoNavigationInQueue) return;
    // go back
    this._saveNavigationStack = false;
    this.tabController.index = this._navigationQueue.last;
    this._navigationQueue.removeLast();
  }

  // reset navigation queue
  resetNavigation() {
    this._navigationQueue.clear();
    this.tabController.index = 0;
  }


  /// Metodi esposti
  void clearStates() {
    _countries.clear();
    _countries.refresh();
  }
  Future<void> loadStates() async {
    try{
      final resp = await StateRepository.loadStati();
      if(resp != null && resp.data != null && resp.data.length > 0){
        List<Country> countries = [];
        for( var jsonCountry in resp.data ){
          final country = Country.fromJson(jsonCountry);
          countries.add(country);
        }
        _countries.clear();
        _countries.assignAll(countries);
        _countries.refresh();
      }
    } catch(error){
      LocalUtils.showError('Errore loadStates', error.toString());
    }
  }

}
