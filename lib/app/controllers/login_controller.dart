import 'package:flutter_getx_scheleton_app/app/data/models/user.dart';
import 'package:flutter_getx_scheleton_app/app/helpers/constants.dart';
import 'package:flutter_getx_scheleton_app/app/helpers/local_utils.dart';
import 'package:flutter_getx_scheleton_app/app/helpers/ui_helper.dart';
import 'package:flutter_getx_scheleton_app/app/routes/app_routes.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {

  final box = GetStorage(appKey);
  RxString _username = RxString('');
  RxString _password = RxString('');
  Rx<User> _userModel = User(id: 0, name: '', surname: '', username: '', email: '').obs;

  User get user => _userModel.value;

  String get username => _username.value;

  String get password => _password.value;

  set username(String value) => _username.value = value;

  set password(String value) => _password.value = value;

  @override
  void onInit() {
    print('LoginController - onInit');
    super.onInit();
    if(_userModel.value.id > 0){
      Get.offAllNamed(AppRoutes.LOGGED);
    }
  }

  @override
  void onReady() {
    print('LoginController - onReady');
    super.onReady();
  }

  @override
  void onClose() {
    print('LoginController - onClose');
    super.onClose();
  }

  @override
  void dispose() {
    print('LoginController - dispose');
    super.dispose();
  }

  //  Metodi esposti
  Future<void> login() async {
    UIHelper.showLoadingDialog();
    await Future.delayed(Duration(seconds: 2));
    _userModel.value = User(id: 0, name: '', surname: '', username: '', email: '');
    _userModel.refresh();
    try {
      if (username.isEmpty) {
        throw new Exception("Lo username non può essere vuoto");
      }
      if (password.isEmpty) {
        throw new Exception("La password non può essere vuoto");
      }
      if (username.trim() == 'admin' && password.trim() == 'admin') {
        var loggedUser = new User(
          id: 1,
          name: 'Alessandro',
          surname: 'Giardini',
          email: 'alessandro.giardini@micra.it',
          username: 'giardini',
        );
        _userModel.value = loggedUser;
        _userModel.refresh();
        UIHelper.hideLoadingDialog();
        Get.offAllNamed(AppRoutes.LOGGED);
      } else {
        throw new Exception("Combinazione username/password errata");
      }
    } catch (e) {
      UIHelper.hideLoadingDialog();
      LocalUtils.showError('Errore Login', e.toString());
    }
  }

  Future<void> logout() async {
    UIHelper.showLoadingDialog();
    await Future.delayed(Duration(seconds: 2));
    _userModel.value = User(id: 0, name: '', surname: '', username: '', email: '');
    _userModel.refresh();
    UIHelper.hideLoadingDialog();
    Get.offAllNamed(AppRoutes.INIT);
  }

}
