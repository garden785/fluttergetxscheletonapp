import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/controllers/home_controller.dart';
import 'package:flutter_getx_scheleton_app/app/helpers/constants.dart';
import 'package:flutter_getx_scheleton_app/app/ui/pages/logged/home_tabs/home_tab_1.dart';
import 'package:get/get.dart';

import 'home_tabs/home_tab_2.dart';
import 'home_tabs/home_tab_3.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (controller.hasNoNavigationInQueue) return true;
        controller.goBackNavigation();
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            appName,
          ),
          actions: [
            GestureDetector(
              onTap: () async {
                await controller.loginController.logout();
              },
              child: Container(
                padding: const EdgeInsets.only(right: 10),
                child: Icon(Icons.logout),
              ),
            ),
          ],
        ),
        body: TabBarView(
          controller: controller.tabController,
          physics: NeverScrollableScrollPhysics(),
          children: [
            HomeTab1(),
            HomeTab2(),
            HomeTab3(),
          ],
        ),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 0.5), //(x,y)
                blurRadius: 1.0,
              ),
            ],
          ),
          child: SafeArea(
            child: TabBar(
              controller: controller.tabController,
              labelStyle: TextStyle(fontSize: 10),
              labelColor: Color.fromRGBO(60, 75, 100, 1.0),
              labelPadding: EdgeInsets.zero,
              unselectedLabelColor: Color.fromRGBO(60, 75, 100, 1.0),
              indicatorSize: TabBarIndicatorSize.label,
              indicatorColor: Color.fromRGBO(60, 75, 100, 1.0),
              tabs: [
                Tab(
                  icon: Icon(
                    Icons.list,
                    size: 18,
                  ),
                  iconMargin: EdgeInsets.only(bottom: 5.0),
                  text: 'Tab 1',
                ),
                Tab(
                  icon: Icon(
                    Icons.access_time,
                    size: 18,
                  ),
                  iconMargin: EdgeInsets.only(bottom: 5.0),
                  text: 'Tab 2',
                ),
                Tab(
                  icon: Icon(
                    Icons.settings,
                    size: 18,
                  ),
                  iconMargin: EdgeInsets.only(bottom: 5.0),
                  text: 'Tab 3',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
