import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/controllers/home_controller.dart';
import 'package:flutter_getx_scheleton_app/app/data/models/country_model.dart';
import 'package:flutter_getx_scheleton_app/app/themes/app_palette.dart';
import 'package:flutter_getx_scheleton_app/app/ui/widgets/ColumBuilder.dart';
import 'package:get/get.dart';

class HomeTab1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _homeController = Get.find<HomeController>();
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          width: double.infinity,
          color: success,
          child: ListView(
            children: [
              Text('Welcome to tab 1'),
              Obx(() {
                if (_homeController.countries.isNotEmpty) {
                  return ElevatedButton(
                    onPressed: () {
                      _homeController.clearStates();
                    },
                    child: Text('Cancella Stati'),
                  );
                } else {
                  return ElevatedButton(
                    onPressed: () async {
                      await _homeController.loadStates();
                    },
                    child: Text('Recupera Stati'),
                  );
                }
              }),
              Obx(() {
                if (_homeController.countries.isNotEmpty) {
                  return Text('Stati caricati correttamente');
                } else {
                  return Text('Stati non presenti');
                }
              }),
              Obx(() {
                if (_homeController.countries.isNotEmpty) {
                  return ColumnBuilder(
                    itemCount: _homeController.countries.length,
                    itemBuilder: (context, index) {
                      return showStateContent(_homeController.countries[index]);
                    },
                  );
                } else {
                  return Container(
                    height: 0,
                  );
                }
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget showStateContent(Country c) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Text(c.nome ?? 'Nome stato null'),
              Row(
                children: [
                  Text('Area: ${c.nomeArea}'),
                  Text('Cod.: ${c.codArea}'),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
