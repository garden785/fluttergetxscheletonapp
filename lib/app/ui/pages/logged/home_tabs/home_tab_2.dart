import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/themes/app_palette.dart';

class HomeTab2 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          color: warning,
          child: Center(
            child: Text('Welcome to tab 2'),
          ),
        ),
      ),
    );
  }
}