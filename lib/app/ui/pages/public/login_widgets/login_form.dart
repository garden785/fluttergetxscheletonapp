import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/controllers/login_controller.dart';
import 'package:get/get.dart';

class LoginForm extends StatelessWidget {
  final formKey = GlobalKey<FormState>();
  final _loginController = Get.find<LoginController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(
                hintText: 'Username',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Il campo non può essere vuoto';
                }
                return null;
              },
              onSaved: (value) {
                _loginController.username = value!;
              },
            ),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Password',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Il campo non può essere vuoto';
                }
                return null;
              },
              onSaved: (value) {
                _loginController.password = value!;
              },
            ),
            ElevatedButton(
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  await _loginController.login();
                }
              },
              child: Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}
