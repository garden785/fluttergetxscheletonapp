import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/controllers/login_controller.dart';
import 'package:flutter_getx_scheleton_app/app/helpers/constants.dart';
import 'package:flutter_getx_scheleton_app/app/ui/pages/public/login_widgets/login_form.dart';
import 'package:get/get.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          appName,
        ),
      ),
      body: LoginForm(),
    );
  }
}
