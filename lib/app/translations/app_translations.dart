import 'package:flutter_getx_scheleton_app/app/translations/en_us/en_us_translations.dart';
import 'package:flutter_getx_scheleton_app/app/translations/it_it/it_it_translations.dart';

abstract class AppTranslations {
  static Map<String, Map<String, String>> translations = {
    'it_IT': {
      ...baseItIt,
    },
    'en_US': {
      ...baseEnUs,
    }
  };
}
