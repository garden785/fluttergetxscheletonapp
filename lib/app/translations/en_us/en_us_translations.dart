final Map<String, String> baseEnUs = {
  'app': 'App',
  'login': 'Login',
  'logout': 'Logout',
  'save': 'Save',
  'edit': 'Edit',
  'delete': 'Delete'
};
