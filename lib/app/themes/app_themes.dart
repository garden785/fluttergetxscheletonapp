import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_palette.dart';
import 'app_text_styles.dart';

final ThemeData lightTheme = ThemeData(
  primarySwatch: Colors.blue,
  accentColor: Colors.deepOrange,
  appBarTheme: AppBarTheme(
    backgroundColor: Colors.blue,
    foregroundColor: Colors.white,
    systemOverlayStyle: SystemUiOverlayStyle.light,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: Colors.deepOrange,
      padding: const EdgeInsets.all(5.0),
    ),
  ),
);



final ThemeData lightThemeOld = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: Colors.white,
  primaryColor: primary,
  accentColor: secondary,
  toggleableActiveColor: secondary,
  appBarTheme: AppBarTheme(
    color: Colors.white,
    iconTheme: IconThemeData(
      color: primary,
    ),
  ),
  cardTheme: CardTheme(
    color: Colors.white,
  ),
  iconTheme: IconThemeData(
    color: primary,
  ),
  textTheme: TextTheme(
    // regular
    headline1: RegularTextStyle.copyWith(color: textColorLight),
    // bold
    headline2: BoldTextStyle.copyWith(color: textColorLight),
    // italic
    headline3: ItalicTextStyle.copyWith(color: textColorLight),
    // light
    headline4: LightTextStyle.copyWith(color: textColorLight),
  ),
);