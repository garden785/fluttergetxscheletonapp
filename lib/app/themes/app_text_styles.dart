import 'package:flutter/material.dart';

// font name
const String FontNameDefault = '';

// font sizes
const LargeTextSize = 20.0;
const MediumTextSize = 18.0;
const SmallTextSize = 14.0;

// text styles
const RegularTextStyle = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: MediumTextSize,
);

const BoldTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: MediumTextSize,
);

const ItalicTextStyle = TextStyle(
  fontStyle: FontStyle.italic,
  fontSize: MediumTextSize,
);

const LightTextStyle = TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: MediumTextSize,
);