import 'package:flutter/material.dart';

const Color backgroundColorLight = white;
const Color backgroundColorDark = Colors.black;
const Color backgroundGrayLight = Color.fromRGBO(235, 237, 239, 1.0);
const Color backgroundGrayDark = Color.fromRGBO(195, 200, 217, 1.0);

// text
const Color textColorLight = primary;
const Color textColorDark = white;

// palette
const Color white = Color.fromRGBO(255, 255, 255, 1.0);
const Color primary = Color.fromRGBO(15, 141, 191, 1.0);
const Color secondary = Color.fromRGBO(213, 229, 242, 1.0);
const Color success = Color.fromRGBO(138, 191, 84, 1.0);
const Color warning = Color.fromRGBO(217, 166, 13, 1.0);
const Color error = Color.fromRGBO(191, 46, 33, 1.0);
