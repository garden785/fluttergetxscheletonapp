import 'package:flutter_getx_scheleton_app/app/bindings/home_binding.dart';
import 'package:flutter_getx_scheleton_app/app/bindings/login_binding.dart';
import 'package:flutter_getx_scheleton_app/app/routes/app_routes.dart';
import 'package:flutter_getx_scheleton_app/app/ui/pages/logged/home_view.dart';
import 'package:flutter_getx_scheleton_app/app/ui/pages/public/login_view.dart';
import 'package:get/get.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: AppRoutes.INIT,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: AppRoutes.LOGGED,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
  ];
}
