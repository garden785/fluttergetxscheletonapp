import 'package:flutter_getx_scheleton_app/app/helpers/json_utils.dart';

class Country {
  int id;
  String? codContinente;
  String? nomeContinente;
  String? codArea;
  String? nomeArea;
  String? codISTAT;
  String? nome;
  String? nomeEN;
  String? codISO3166A3;

  Country({
    required this.id,
    this.codContinente,
    this.nomeContinente,
    this.codArea,
    this.nomeArea,
    this.codISTAT,
    this.nome,
    this.nomeEN,
    this.codISO3166A3,
  });


  @override
  String toString() {
    return this.toJson().toString();
  }

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
      id: JsonUtils.convertJsonField(
        inputValue: json.containsKey('id') ? json['id'] : 0,
        outputType: 'int',
        outputDefaultValue: 0,
      ),
      codContinente: JsonUtils.convertJsonField(
        inputValue: json.containsKey('cod_continente') ? json['cod_continente'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      nomeContinente: JsonUtils.convertJsonField(
        inputValue: json.containsKey('nome_continente') ? json['nome_continente'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      codArea: JsonUtils.convertJsonField(
        inputValue: json.containsKey('cod_area') ? json['cod_area'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      nomeArea: JsonUtils.convertJsonField(
        inputValue: json.containsKey('nome_area') ? json['nome_area'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      codISTAT: JsonUtils.convertJsonField(
        inputValue: json.containsKey('cod_istat') ? json['cod_istat'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      nome: JsonUtils.convertJsonField(
        inputValue: json.containsKey('nome') ? json['nome'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      nomeEN: JsonUtils.convertJsonField(
        inputValue: json.containsKey('nome_en') ? json['nome_en'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      codISO3166A3: JsonUtils.convertJsonField(
        inputValue: json.containsKey('cod_iso3166_a3') ? json['cod_iso3166_a3'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
    );
  }

  Map<String, dynamic> toJson() => {
    'id': this.id,
    'codContinente': this.codContinente,
    'nomeContinente': this.nomeContinente,
    'codArea': this.codArea,
    'nomeArea': this.nomeArea,
    'codISTAT': this.codISTAT,
    'nome': this.nome,
    'nomeEN': this.nomeEN,
    'codISO3166_A3': this.codISO3166A3,
  };

}
