import 'package:flutter_getx_scheleton_app/app/helpers/json_utils.dart';

class User {
  //  Properties
  int id;
  String name;
  String surname ;
  String username;
  String email;

  //  Costruttore
  User({
    required this.id,
    required this.name,
    required this.surname,
    required this.username,
    required this.email,
  });

  //  Metodo per la creazione di un oggetto di tipo User partendo da un json
  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: JsonUtils.convertJsonField(
        inputValue: json.containsKey('id') ? json['id'] : 0,
        outputType: 'int',
        outputDefaultValue: 0,
      ),
      name: JsonUtils.convertJsonField(
        inputValue: json.containsKey('name') ? json['name'] : 0,
        outputType: 'string',
        outputDefaultValue: '',
      ),
      surname: JsonUtils.convertJsonField(
        inputValue: json.containsKey('surname') ? json['surname'] : 0,
        outputType: 'string',
        outputDefaultValue: '',
      ),
      username: JsonUtils.convertJsonField(
        inputValue: json.containsKey('username') ? json['username'] : 0,
        outputType: 'string',
        outputDefaultValue: '',
      ),
      email: JsonUtils.convertJsonField(
        inputValue: json.containsKey('email') ? json['email'] : 0,
        outputType: 'string',
        outputDefaultValue: '',
      ),
    );
  }

  //  Metodo per creare la rappresentazione json di un oggetto di tipo User
  Map<String, dynamic> toJson() => {
        'id': this.id,
        'name': this.name,
        'surname': this.surname,
        'username': this.username,
        'email': this.email,
      };
}
