import 'package:flutter_getx_scheleton_app/app/helpers/json_utils.dart';

class HttpJsonResponse {

  bool success = true;
  String? message;
  dynamic data;
  int total = 0;

  HttpJsonResponse({
    required this.success,
    this.message,
    this.data,
    this.total = 0
  });

  factory HttpJsonResponse.fromJson(Map<String, dynamic> json) {
    return HttpJsonResponse(
      success: JsonUtils.convertJsonField(
        inputValue: json.containsKey('success') ? json['success'] : 0,
        outputType: 'bool',
        outputDefaultValue: false,
      ),
      message: JsonUtils.convertJsonField(
        inputValue: json.containsKey('message') ? json['message'] : null,
        outputType: 'string',
        outputDefaultValue: null,
      ),
      data: json.containsKey('data') ? json['data'] : null,
      total: JsonUtils.convertJsonField(
        inputValue: json.containsKey('total') ? json['total'] : 0,
        outputType: 'int',
        outputDefaultValue: 0,
      ),
    );
  }

  Map<String, dynamic> toJson() => {
    'success': this.success,
    'message': this.message,
    'data': this.data,
    'total': this.total,
  };

}