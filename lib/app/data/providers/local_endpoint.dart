class LocalEndpoint{


  static const apiScheme = 'https';
  static const apiHost = 'demo.micrapg.com';
  //static const apiPort = 80;
  static const apiContext = '/gclex20/SmartDriver/PBP_APP/';

  // static const apiScheme = 'https';
  // static const apiHost = 'www.alessandrogiardini.it';
  // //static const apiPort = 80;
  // static const apiContext = '/agt/restServices/';

  Uri uri(String path, {Map<String, dynamic>? queryParameters}){
    final uri = Uri(
      scheme: apiScheme,
      host: apiHost,
      //port: apiPort,
      path: apiContext + path,
      queryParameters: queryParameters,
    );
    print(uri.toString());
    return uri;
  }
}