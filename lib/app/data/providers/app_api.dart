import 'dart:async';

import 'package:flutter_getx_scheleton_app/app/helpers/constants.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

import 'local_endpoint.dart';

class AppAPI {
  //  Properties
  late LocalEndpoint _endpoint;
  static Duration defaultRequestTimeout = Duration(seconds: 15);

  //  Singleton
  static final AppAPI _singleton = AppAPI._internal();

  factory AppAPI() {
    return _singleton;
  }

  AppAPI._internal() {
    _endpoint = LocalEndpoint();
  }

  //  Metodi di supporto
  Map<String, String> getRequestHeaders({bool addHeaderAuthorization = false, bool addHeaderContentJson = false, String? addHeaderCustomContent}) {
    var reqHeaders = <String, String>{};
    reqHeaders.putIfAbsent('Accept', () => 'application/json');
    reqHeaders.putIfAbsent('Accept-Encoding', () => 'gzip');
    if (addHeaderAuthorization) {
      reqHeaders.putIfAbsent('Authorization', () => 'Bearer ' + this.getJwtToken());
    }
    if (addHeaderContentJson) {
      reqHeaders.putIfAbsent('Content-Type', () => 'application/json');
    } else if (addHeaderCustomContent != null && addHeaderCustomContent.isNotEmpty) {
      reqHeaders.putIfAbsent('Content-Type', () => addHeaderCustomContent);
    }
    return reqHeaders;
  }
  http.Response getServerResponse(http.Response response){
    if (response.statusCode == 200) {
      return response;
    } else if (response.statusCode == 403) {
      throw Exception('Non si dispone delle autorizzazioni necessarie per accedere alla risorsa');
    } else if (response.statusCode == 404) {
      throw Exception('Risorsa non trovata');
    } else if (response.statusCode == 415) {
      throw Exception('Unsupported media type');
    } else {
      throw Exception('Errore nel recupero dei dati');
    }
  }
  String getJwtToken() {
    return GetStorage(appKey).read(jwtKey);
  }

  //  Metodi esposti
  Future<http.Response> get(
    String serviceName, {
    Map<String, dynamic>? queryParameters,
    bool addAuthHeader = false,
    String? customContentHeader,
    Duration? execTimeout,
  }) async {
    var url = this._endpoint.uri(serviceName, queryParameters: queryParameters);
    try {
      var response = await http
          .get(
            url,
            headers: this.getRequestHeaders(
              addHeaderAuthorization: addAuthHeader,
              addHeaderContentJson: false,
              addHeaderCustomContent: customContentHeader,
            ),
          )
          .timeout(execTimeout ?? defaultRequestTimeout);
      return getServerResponse(response);
    } on TimeoutException catch (timeExc) {
      return Future.error(timeExc);
    } on Error catch (err) {
      return Future.error(err);
    }
  }

  Future<http.Response> post(
    String serviceName, {
    dynamic body,
    bool addAuthHeader = false,
    String? customContentHeader,
    Duration? execTimeout,
  }) async {
    var url = this._endpoint.uri(serviceName);
    try {
      var response = await http
          .post(
            url,
            headers: this.getRequestHeaders(
              addHeaderAuthorization: addAuthHeader,
              addHeaderContentJson: false,
              addHeaderCustomContent: customContentHeader,
            ),
            body: body,
          )
          .timeout(execTimeout ?? defaultRequestTimeout);
      return getServerResponse(response);
    } on TimeoutException catch (timeExc) {
      return Future.error(timeExc);
    } on Error catch (err) {
      return Future.error(err);
    }
  }

  Future<http.Response> put(
    String serviceName, {
    dynamic body,
    bool addAuthHeader = false,
    String? customContentHeader,
    Duration? execTimeout,
  }) async {
    var url = this._endpoint.uri(serviceName);
    try {
      var response = await http
          .put(
            url,
            headers: this.getRequestHeaders(
              addHeaderAuthorization: addAuthHeader,
              addHeaderContentJson: false,
              addHeaderCustomContent: customContentHeader,
            ),
            body: body,
          )
          .timeout(execTimeout ?? defaultRequestTimeout);
      return getServerResponse(response);
    } on TimeoutException catch (timeExc) {
      return Future.error(timeExc);
    } on Error catch (err) {
      return Future.error(err);
    }
  }

  Future<http.Response> delete(
    String serviceName, {
    dynamic body,
    bool addAuthHeader = false,
    String? customContentHeader,
    Duration? execTimeout,
  }) async {
    var url = this._endpoint.uri(serviceName);
    try {
      var response = await http
          .delete(
            url,
            headers: this.getRequestHeaders(
              addHeaderAuthorization: addAuthHeader,
              addHeaderContentJson: false,
              addHeaderCustomContent: customContentHeader,
            ),
            body: body,
          )
          .timeout(execTimeout ?? defaultRequestTimeout);
      return getServerResponse(response);
    } on TimeoutException catch (timeExc) {
      return Future.error(timeExc);
    } on Error catch (err) {
      return Future.error(err);
    }
  }
}
