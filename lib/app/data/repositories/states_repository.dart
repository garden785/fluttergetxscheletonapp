import 'dart:convert';
import 'package:flutter_getx_scheleton_app/app/data/models/http_json_response.dart';
import 'package:flutter_getx_scheleton_app/app/data/providers/app_api.dart';

class StateRepository{

  static Future<HttpJsonResponse?> loadStati() async {
    HttpJsonResponse jsonResponse;
    try{
      final response = await AppAPI().get('/asdasd/getStati.php');
      final responseData = json.decode(response.body);
      jsonResponse = HttpJsonResponse.fromJson(responseData);
      if(jsonResponse.success){
        return jsonResponse;
      } else {
        throw Exception(jsonResponse.message);
      }
    } catch(error){
      throw Exception(error.toString());
    }
  }

}