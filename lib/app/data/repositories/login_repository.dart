import 'dart:convert';

import 'package:flutter_getx_scheleton_app/app/data/models/http_json_response.dart';
import 'package:flutter_getx_scheleton_app/app/data/providers/app_api.dart';

class LoginRepository{

  static Future<HttpJsonResponse?> loginUser({required String username, required String password}) async {
    HttpJsonResponse jsonResponse;
    try{
      username = username.trim();
      password = password.trim();
      final loginCredentials = '{"username":"$username", "password": "$password"}';
      final response = await AppAPI().post('rest/app/initials/login', body: loginCredentials, customContentHeader: 'application/json');
      final responseData = json.decode(response.body);
      jsonResponse = HttpJsonResponse.fromJson(responseData);
      if(jsonResponse.success){
        return jsonResponse;
      } else {
        throw Exception(jsonResponse.message);
      }
    } catch(error){
      throw Exception(error.toString());
    }
  }

  static Future<HttpJsonResponse> userData() async {
    //
    HttpJsonResponse jsonResponse;
    try{
      final response = await AppAPI().post('rest/app/initials/userData', addAuthHeader: true);
      final responseData = json.decode(response.body);
      jsonResponse = HttpJsonResponse.fromJson(responseData);
      if(jsonResponse.success){
        return jsonResponse;
      } else {
        throw Exception(jsonResponse.message);
      }
    } catch(error){
      throw Exception(error.toString());
    }
  }

}