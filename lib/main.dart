import 'package:flutter/material.dart';
import 'package:flutter_getx_scheleton_app/app/helpers/constants.dart';
import 'package:flutter_getx_scheleton_app/app/scheleton_app.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  await GetStorage.init(appKey);
  runApp(ScheletonApp());
}
